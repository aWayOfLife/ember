import { Component } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { title } from 'process';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

interface Post{
  title:string;
  content:string;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  postCollection: AngularFirestoreCollection<Post>;
  posts: Observable<Post[]>;
  title:string;
  content:string;

  constructor(private store: AngularFirestore){}

  ngOnInit(){
    this.postCollection = this.store.collection('posts');
    this.posts = this.postCollection.valueChanges();


  }

  addPost(){
    this.store.collection('posts').add({title:this.title, content:this.content});
  }
}
