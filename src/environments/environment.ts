// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD19gVxGIwHp7mDvRqFX6bKXcRzZhuhgnI",
    authDomain: "ember-12a32.firebaseapp.com",
    databaseURL: "https://ember-12a32.firebaseio.com",
    projectId: "ember-12a32",
    storageBucket: "ember-12a32.appspot.com",
    messagingSenderId: "533872853853",
    appId: "1:533872853853:web:129941da061764a5271eb4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
